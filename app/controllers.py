from flask import render_template, redirect, url_for, request, Response, Flask
from app import app, db
from twilio import twiml
from models import Call
from twilio.rest import TwilioRestClient
from twilio import twiml
import datetime
import config

client = TwilioRestClient(config.ACCOUNT_SID , config.AUTH_TOKEN)

def wrap_xml_content_type(twiml_response):
    return Response(str(twiml_response), mimetype='text/xml')

@app.route('/handleCall', methods=['GET', 'POST'])
def prompt_ivr():
    change_call_status()
    twiml_response = twiml.Response()
    with twiml_response.gather(numDigits=1, action="/handleInput", method="POST") as gather:
        gather.say("""This is a call from Twillio's Voice Notification demo. 
            Press 1 to confirm. 
            Press 2 to decline. 
            Press 3 to hear this message again""", voice="alice")
    return wrap_xml_content_type(twiml_response)

def get_current_call():
    return Call.query.filter(Call.call_sid == request.values.get('CallSid', None)).first()

def change_call_status():
    current_call_internal = get_current_call()
    current_call_internal.call_status = request.values.get('CallStatus', None)
    db.session.add(current_call_internal)
    db.session.commit()

@app.route('/handleInput', methods=['GET', 'POST'])
def handle_input():
    twiml_response = twiml.Response()
    digits = request.values.get('Digits', None)
    return handle_digit(digits)

def set_call_selection(selected_option):
    current_call_internal = get_current_call()
    current_call_internal.selection = selected_option
    db.session.add(current_call_internal)
    db.session.commit()

def digit_1():
    set_call_selection("confirmed")
    twiml_response = twiml.Response()
    twiml_response.say("Thank you for confirming")
    return wrap_xml_content_type(twiml_response)

def digit_2():
    set_call_selection("declined")
    twiml_response = twiml.Response()
    twiml_response.say("Decline noted. Thank you.")
    return wrap_xml_content_type(twiml_response)

def digit_3():
    twiml_response = twiml.Response()
    twiml_response.redirect("/handleCall")
    return wrap_xml_content_type(twiml_response)

def digit_default():
    twiml_response = twiml.Response()
    twiml_response.say("Sorry, I don't understand that.")
    return wrap_xml_content_type(twiml_response)

def handle_digit(digit):
    if digit == "1":
        return digit_1()
    if digit == "2":
        return digit_2()
    if digit == "3":
        return digit_3()
    else:
        return digit_default()

@app.route('/handleCallComplete', methods=['GET', 'POST'])
def handle_call_complete():
    change_call_status()

@app.route('/makeOutboundCall', methods = ['GET', 'POST'])
def make_outbound_calls():
    make_outbound_call('number0')
    make_outbound_call('number1')
    make_outbound_call('number2')
    make_outbound_call('number3')
    make_outbound_call('number4')
    make_outbound_call('number5')
    make_outbound_call('number6')
    make_outbound_call('number7')
    make_outbound_call('number8')
    make_outbound_call('number9')
    return redirect(url_for('show_results'))

def make_outbound_call(number_name):
    if request.form[number_name]:
        rest_response = client.calls.create(
            to=request.form[number_name], 
            from_=config.TWILIO_PHONE_NUMBER, 
            url=request.url_root + "handleCall",
            status_callback=request.url_root + "handleCallComplete")
        save_call(rest_response)

def save_call(rest_response):
    to_number_formatted = format_to_number(rest_response.to)
    remove_oldest_call()
    call = Call(
        call_sid = rest_response.sid, 
        call_status = rest_response.status,
        to_number = to_number_formatted,
        start_time = datetime.datetime.now(),
        selection = "NA"
    )
    db.session.add(call)
    db.session.commit()

def remove_oldest_call():
    calls = Call.query.all()
    oldest_call = Call()
    if len(calls) >= 20:
        oldest_call = min(calls, key=lambda x:x.start_time)
        db.session.delete(oldest_call)
        db.session.commit()

def format_to_number(number):
    blank_characters = ''
    for x in range(0, len(number)-5):
        blank_characters = blank_characters + 'X'
    return '+' + blank_characters + number[-4:]














































