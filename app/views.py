from flask import render_template, redirect, url_for, request, flash
from app import app, db
from models import Call
from operator import itemgetter, attrgetter

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html',
        title = 'Voice Notification Demo')


@app.route('/results', methods = ['GET', 'POST'])
def show_results():
    unsorted_calls = Call.query.all()
    calls = sorted(unsorted_calls, key=attrgetter('start_time'), reverse=True)
    return render_template('results.html',
        calls = calls,
        title = 'Voice Notification Results')










































